package cql.ecci.ucr.ac.cr.miexamen01;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import android.app.Fragment;
import android.widget.Toast;

import cql.ecci.ucr.ac.cr.miexamen01.Model.CountryOCDE;

public class CountryFragment extends Fragment{

    private CountryOCDE country;
    // The onCreateView method is called when Fragment should create its View object hierarchy,
    // either dynamically or via XML layout inflation.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        Bundle bundle = getArguments();
        if(bundle != null){
            country = bundle.getParcelable("country");

        }
        return inflater.inflate(R.layout.fragment_country_details, parent, false);
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        TextView title = (TextView) view.findViewById(R.id.fragment_name);
        TextView details = (TextView) view.findViewById(R.id.fragment_description);
        ImageView image  = (ImageView) view.findViewById(R.id.fragment_icon);
        ImageView map  = (ImageView) view.findViewById(R.id.fragment_mapIcon);
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("click", "clicked on map : " + country.getName());
                Intent intent = new Intent(v.getContext(), MapsActivity.class);
                intent.putExtra("lat", country.getLatitude());
                intent.putExtra("lng", country.getLongitude());
                intent.putExtra("name", country.getName());
                startActivity(intent);
            }
        });
        title.setText(country.getName());
        details.setText(country.getIso() + " " + country.getType());
        image.setImageResource(country.getFlag_num());
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);
    }
}