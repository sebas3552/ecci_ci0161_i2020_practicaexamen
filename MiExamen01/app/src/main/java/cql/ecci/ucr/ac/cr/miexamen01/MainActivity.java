package cql.ecci.ucr.ac.cr.miexamen01;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import cql.ecci.ucr.ac.cr.miexamen01.Model.CountryOCDE;
import cql.ecci.ucr.ac.cr.miexamen01.Model.DataAccess;
import cql.ecci.ucr.ac.cr.miexamen01.Model.DatabaseContract;
import cql.ecci.ucr.ac.cr.miexamen01.Model.DeploymentScript;

public class MainActivity extends AppCompatActivity {
    private ListView list;
    private List<String> itemname;
    private List<Integer> imgId;
    private List<String> itemdescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DeploymentScript.Run(getApplicationContext());

        itemname = new ArrayList<>();
        imgId = new ArrayList<>();
        itemdescription = new ArrayList<>();

        List<CountryOCDE> countries = CountryOCDE.selectAll(getApplicationContext());
        for(CountryOCDE country : countries){
            itemname.add(country.getName());
            imgId.add(country.getFlag_num());
            itemdescription.add(country.getType());
        }

        CustomListAdapter adapter = new CustomListAdapter(this, itemname, imgId, itemdescription);
        list = (ListView) findViewById(R.id.list);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                String selectedItem = itemname.get(position);
                CountryOCDE selected = CountryOCDE.select(getApplicationContext(), DatabaseContract.CountryOCDE.NAME + " = '" + selectedItem + "'");
                if(selected != null) {
                    Bundle bundle = new Bundle();
                    CountryFragment fragment = new CountryFragment();
                    bundle.putParcelable("country", selected);
                    fragment.setArguments(bundle);
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.country_details, fragment);
                    ft.commit();
                }else{
                    Toast.makeText(getApplicationContext(), "Error! Country not found in database.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
