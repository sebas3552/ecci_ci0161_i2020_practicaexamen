package cql.ecci.ucr.ac.cr.miexamen01;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

public class CustomListAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final List<String> itemName;
    private final List<Integer> imgId;
    private final List<String> itemDescription;

    public CustomListAdapter(Activity context, List<String> itemname, List<Integer> imgid, List<String> itemdescription){
        super(context, R.layout.custom_list, itemname);

        this.context = context;
        itemName = itemname;
        imgId = imgid;
        itemDescription = itemdescription;
    }

    public View getView(int position, View view, ViewGroup parent){
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.custom_list, null, true);

        TextView nombre = rowView.findViewById(R.id.name);
        ImageView imagen = rowView.findViewById(R.id.icon);
        TextView descripcion = rowView.findViewById(R.id.description);

        nombre.setText(itemName.get(position));
        imagen.setImageResource(imgId.get(position));
        descripcion.setText(itemDescription.get(position));

        return rowView;
    }
}
