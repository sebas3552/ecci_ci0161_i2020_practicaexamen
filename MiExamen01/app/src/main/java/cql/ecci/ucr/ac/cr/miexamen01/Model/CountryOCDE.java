package cql.ecci.ucr.ac.cr.miexamen01.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class CountryOCDE implements Parcelable {
    private String iso;
    private String name;
    private int population;
    private String internet;
    private double latitude;
    private double longitude;
    private double sia;
    private double sd;
    private double sl;
    private double st;
    private String type;
    private int flag_num;

    public CountryOCDE(String iso, String name, int population, String internet, double latitude, double longitude, double sia, double sd, double sl, double st, String type, int flag_num) {
        this.iso = iso;
        this.name = name;
        this.population = population;
        this.internet = internet;
        this.latitude = latitude;
        this.longitude = longitude;
        this.sia = sia;
        this.sd = sd;
        this.sl = sl;
        this.st = st;
        this.type = type;
        this.flag_num = flag_num;
    }

    public CountryOCDE() {
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public String getInternet() {
        return internet;
    }

    public void setInternet(String internet) {
        this.internet = internet;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getSia() {
        return sia;
    }

    public void setSia(double sia) {
        this.sia = sia;
    }

    public double getSd() {
        return sd;
    }

    public void setSd(double sd) {
        this.sd = sd;
    }

    public double getSl() {
        return sl;
    }

    public void setSl(double sl) {
        this.sl = sl;
    }

    public double getSt() {
        return st;
    }

    public void setSt(double st) {
        this.st = st;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getFlag_num() {
        return flag_num;
    }

    public void setFlag_num(int flag_num) {
        this.flag_num = flag_num;
    }


    public long insert(Context context){
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.CountryOCDE.ISO, getIso());
        values.put(DatabaseContract.CountryOCDE.NAME, getName());
        values.put(DatabaseContract.CountryOCDE.POPULATION, getPopulation());
        values.put(DatabaseContract.CountryOCDE.INTERNET, getInternet());
        values.put(DatabaseContract.CountryOCDE.LATITUDE, getLatitude());
        values.put(DatabaseContract.CountryOCDE.LONGITUDE, getLongitude());
        values.put(DatabaseContract.CountryOCDE.LONGITUDE, getLongitude());
        values.put(DatabaseContract.CountryOCDE.SIA, getSia());
        values.put(DatabaseContract.CountryOCDE.SD, getSd());
        values.put(DatabaseContract.CountryOCDE.SL, getSl());
        values.put(DatabaseContract.CountryOCDE.ST, getSt());
        values.put(DatabaseContract.CountryOCDE.TYPE, getType());
        values.put(DatabaseContract.CountryOCDE.FLAG_NUM, getFlag_num());

        DataAccess dataAccess = DataAccess.getInstance(context);
        dataAccess.open();
        long result = dataAccess.insert(DatabaseContract.CountryOCDE.TABLE_NAME, values);
        dataAccess.close();
        return result;
    }



    public static List<CountryOCDE> selectAll(Context context){
        DataAccess dataAccess = DataAccess.getInstance(context);
        dataAccess.open();

        Cursor cursor = dataAccess.selectAll(
                DatabaseContract.CountryOCDE.TABLE_NAME
        );
        cursor.moveToFirst();
        List<CountryOCDE> countries = new ArrayList<>();
        while (!cursor.isAfterLast()) {
            countries.add( new CountryOCDE(
                    cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.ISO)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.NAME)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.POPULATION)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.INTERNET)),
                    cursor.getDouble(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.LATITUDE)),
                    cursor.getDouble(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.LONGITUDE)),
                    cursor.getDouble(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.SIA)),
                    cursor.getDouble(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.SD)),
                    cursor.getDouble(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.SL)),
                    cursor.getDouble(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.ST)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.TYPE)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.FLAG_NUM))
            ));
            cursor.moveToNext();
        }
        cursor.close();
        dataAccess.close();
        Log.d("countryRead", "All the countries had been read from database.");
        return countries;
    }

    public static CountryOCDE select(Context context, String where){
        DataAccess dataAccess = DataAccess.getInstance(context);
        dataAccess.open();

        Cursor cursor = dataAccess.select(
                null,
                DatabaseContract.CountryOCDE.TABLE_NAME,
                where
        );
        cursor.moveToFirst();
        CountryOCDE country = new CountryOCDE(
            cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.ISO)),
            cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.NAME)),
            cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.POPULATION)),
            cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.INTERNET)),
            cursor.getDouble(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.LATITUDE)),
            cursor.getDouble(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.LONGITUDE)),
            cursor.getDouble(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.SIA)),
            cursor.getDouble(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.SD)),
            cursor.getDouble(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.SL)),
            cursor.getDouble(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.ST)),
            cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.TYPE)),
            cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseContract.CountryOCDE.FLAG_NUM))
        );
        cursor.close();
        dataAccess.close();
        return country;
    }

    protected CountryOCDE(Parcel in) {
        iso = in.readString();
        name = in.readString();
        population = in.readInt();
        internet = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        sia = in.readDouble();
        sd = in.readDouble();
        sl = in.readDouble();
        st = in.readDouble();
        type = in.readString();
        flag_num = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(iso);
        dest.writeString(name);
        dest.writeInt(population);
        dest.writeString(internet);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeDouble(sia);
        dest.writeDouble(sd);
        dest.writeDouble(sl);
        dest.writeDouble(st);
        dest.writeString(type);
        dest.writeInt(flag_num);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CountryOCDE> CREATOR = new Parcelable.Creator<CountryOCDE>() {
        @Override
        public CountryOCDE createFromParcel(Parcel in) {
            return new CountryOCDE(in);
        }

        @Override
        public CountryOCDE[] newArray(int size) {
            return new CountryOCDE[size];
        }
    };
}
