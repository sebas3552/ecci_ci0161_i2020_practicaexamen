package cql.ecci.ucr.ac.cr.miexamen01.Model;

import android.provider.BaseColumns;

/** @class DatabaseContract
 *  This class defines the string constants and statements to define the database model in local storage
 *  through SQLite, following the inner-class hierarchy Module > Tables > Columns and Statements.
 * */
public class DatabaseContract {

    private DatabaseContract () {}

    private static final String COMMA = ", ";
    private static final String TEXT_TYPE = " TEXT ";
    private static final String INTEGER_TYPE = " INTEGER ";
    private static final String REAL_TYPE = " REAL ";
    private static final String PK = " PRIMARY KEY ";
    private static final String FK = " FOREIGN KEY ";
    private static final String REF = " REFERENCES ";
    private static final String NULLABLE = " null ";

    public static class CountryOCDE{
        public static final String TABLE_NAME = "Country";

        public static final String ISO = "Iso";
        public static final String NAME = "Name";
        public static final String POPULATION = "Population";
        public static final String INTERNET = "Internet";
        public static final String LATITUDE = "Latitude";
        public static final String LONGITUDE = "Longitude";
        /*Students Internet Access*/
        public static final String SIA = "Sia";
        /*Students Desktop*/
        public static final String SD = "Sd";
        /*Students Laptop*/
        public static final String SL = "Sl";
        /*Students Tablet*/
        public static final String ST = "St";
        public static final String TYPE = "Type";
        public static final String FLAG_NUM = "Flag_num";

        /*Create statement*/

        public static final String SQL_CREATE_COUNTRY =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        ISO + TEXT_TYPE + PK + COMMA +
                        NAME + TEXT_TYPE + COMMA +
                        POPULATION + INTEGER_TYPE + COMMA +
                        INTERNET + TEXT_TYPE + COMMA +
                        LATITUDE + REAL_TYPE + COMMA +
                        LONGITUDE + REAL_TYPE + COMMA +
                        SIA + REAL_TYPE + COMMA +
                        SD + REAL_TYPE + COMMA +
                        SL + REAL_TYPE + COMMA +
                        ST + REAL_TYPE + COMMA +
                        TYPE + TEXT_TYPE + COMMA +
                        FLAG_NUM + INTEGER_TYPE +
                        ")";

        public static final String SQL_DELETE_COUNTRY =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public static String[] DeploymentScript = {
        CountryOCDE.SQL_DELETE_COUNTRY,
        CountryOCDE.SQL_CREATE_COUNTRY
    };

}
