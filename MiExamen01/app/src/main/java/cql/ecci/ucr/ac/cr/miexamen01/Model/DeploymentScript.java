package cql.ecci.ucr.ac.cr.miexamen01.Model;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import cql.ecci.ucr.ac.cr.miexamen01.R;

public class DeploymentScript {
    public static void Run(Context context){
        clearDatabase(context);
        createCountries(context);
    }

    private static void createCountries(Context context) {
        DataAccess db = new DataAccess(context);
        List<CountryOCDE> list = new ArrayList<>();
        String[] iso = {"CL", "DE", "AS", "JP", "KR", "MX", "ES", "CR"};
        String[] name = {"Chile", "Germany", "Israel", "Japan", "Korea", "Mexico", "Spain", "Costa Rica"};
        int[] population = {18, 81, 8, 127, 50, 120, 46, 5};
        String[] internet = {".cl", ".de", ".il", ".jp", ".kr", ".mx", ".es", "cr"};
        double[] latitude = {-33.403472, 51.565763, 31.002184, 36.636080, 36.466042, 25.487177, 40.647801, 9.920679};
        double[] longitude = {-71.365730, 10.494769, 34.848583, 139.042399, 127.940078, -102.612236, -3.914577, -84.125663};
        double[] sia = {76.28, 98.62, 91.54, 88.55, 97.23, 47.41, 94.73, 66.39};
        double[] sd = {54.31, 66.76, 50.92, 53.44, 38.58, 55.62, 64.24, 48.78};
        double[] sl = {21.07, 19.11, 14.77, 11.76, 8.65, 12.96, 23.30, 26.42};
        double[] st = {5.88, 2.56, 8.04, 5.52, 2.35, 5.82, 3.83, 5.06};
        String[] type = {"Member", "Member", "Member", "Member", "Member", "Member", "Member", "Partner"};
        int[] imgId = {R.drawable.cl, R.drawable.de, R.drawable.il, R.drawable.jp, R.drawable.kr, R.drawable.mx, R.drawable.es, R.drawable.cr};

        for (int i = 0; i < iso.length; ++i) {
            list.add(new CountryOCDE(iso[i], name[i], population[i], internet[i], latitude[i], longitude[i], sia[i], sd[i], sl[i], st[i], type[i], imgId[i]));
        }
        for(CountryOCDE f : list){
            f.insert(context);
        }
        db.close();
        Log.d("faculties", "Faculties were inserted in database.");
    }

    private static void clearDatabase(Context context){
        DataAccess db = new DataAccess(context);
        db.resetDatabase();
        Log.d("reset", "Database was reset");
    }
}
